

// Licence: NetBSD; Free Software
// Autologin using rc.conf



#include <stdio.h>
#include <string.h>
#include <stdlib.h>  
#include <dirent.h>
#include <sys/stat.h>
#include <dirent.h>
#include <sys/types.h>
#if defined(__linux__) //linux
#define OSTYPE 1
#elif defined(_WIN32)
#define OSTYPE 9
#elif defined(_WIN64)
#define OSTYPE 8
#elif defined(__unix__) 
#define OSTYPE 2
// netbsd will give 2
#define PATH_MAX 2500
#else
#define OSTYPE 3
#endif




int fcheckfile(const char *a_option)
{
	char dir1[PATH_MAX]; 
	char *dir2;
	DIR *dip;
	strncpy( dir1 , "",  PATH_MAX  );
	strncpy( dir1 , a_option,  PATH_MAX  );

	struct stat st_buf; 
	int status; 
	int fileordir = 0 ; 

	status = stat ( dir1 , &st_buf);
	if (status != 0) {
		fileordir = 0;
	}
	FILE *fp2check = fopen( dir1  ,"r");
	if( fp2check ) {
		fileordir = 1; 
		fclose(fp2check);
	} 

	if (S_ISDIR (st_buf.st_mode)) {
		fileordir = 2; 
	}
	return fileordir;
}



char *fgetusername(char *name)
{
	char *base = name;
	while (*name)
	{
		if (*name++ == '=')
		{
			base = name;
		}
	}
	return (base);
}



char autologin_username[128];
void grepfile( char *filesource , char *sstring)
{
	FILE *source; 
	int c,j ;
	char lline[PATH_MAX]; 
	char mysstring[PATH_MAX]; 
	strncpy( mysstring, sstring,  PATH_MAX); int pcc=0;
	int begin;
	j = 0;
	printf( "FILENAME: %s\n" , filesource ); 
	source = fopen( filesource , "r");
	if ( fcheckfile( filesource ) == 1 ) 
	{
		{    c = fgetc(source);
			while( c != EOF )
			{
				if ( c != '\n' ) 
					lline[j++] = c;

				begin = 0;
				if ( c == '\n' )
				{  
					begin = 1;
					lline[j]='\0';

					if ( lline[0] != '#' ) 
					if ( strstr( lline, mysstring ) != 0 )
					{
						printf( "%s\n", lline );
						strncpy( autologin_username, fgetusername( lline  ) , 128 ); 
					}

					j = 0;
					lline[j]='\0';
				}
				c = fgetc(source);
			}
			fclose(source);
		}
	}
}









int main( int argc, char *argv[])
{
	if ( argc == 2)
	if ( ( strcmp( argv[1] , "--help" ) ==  0 ) 
	|| ( strcmp( argv[1] ,   "-help" ) ==  0 ) 
	|| ( strcmp( argv[1] ,   "help" ) ==  0 ) )
	{
			printf( " /etc/ttys file with the line: console \"/usr/local/bin/autologin Pc\" vt100 on secure \n" ); 
			printf( " or: ttyE0 \"/usr/local/bin/autologin Pc\" vt100 on secure \n" ); 
			printf( " Usage: /usr/local/bin/autologin \n" ); 
			printf( " Add to /etc/rc.conf the content: autologin_user=myusername\n" );
			return 0;
		}

	char strcmd[PATH_MAX];
	printf( "=================\n"); 
	printf( " BSD Autologin   \n"); 
	printf( "=================\n"); 
	strncpy( autologin_username, "" , 128 );

	if ( fcheckfile( "/etc/rc.conf" ) == 1 ) 
	{

		printf(    "=============\n"); 
		grepfile(  "/etc/rc.conf" , "autologin_user" ); 
		printf(    " ==> Found: Username for autologin: (%s)\n" , autologin_username ); 
		printf(    "=============\n"); 
		snprintf( strcmd , PATH_MAX , "  PATH='/bin:/sbin:/usr/bin:/usr/sbin:/usr/X11R7/bin:/usr/pkg/bin:/usr/pkg/sbin:/usr/games:/usr/local/bin:/usr/local/sbin'  su %s -c  '  cd ;  /usr/X11R7/bin/startx -- :0  ; sleep 5' ",  autologin_username );  
		printf( "command: %s\n", strcmd ); 
		system( strcmd ); 
	}
	return 0; 
}





