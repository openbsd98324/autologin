# autologin

# Introduction

Little C code, which allows at boot to start the command startx on your modern NetBSD system, nicely. 
This will start your Desktop directly at boot. 

This was tested on the Raspberry PI 3, model b and b+.
(see this gitlab armv7m)

# Installation

cc autologin.c -o /usr/local/bin/autologin

Change /etc/ttys


# Config

Modify /etc/rc.conf

and add :

autologin_user=netbsd   

(for instance)


# Extras

To set bash as default, example:


chpass netbsd


and set to SHELL:   /usr/pkg/bin/bash 


This will become the default SHELL for your given user (e.g. netbsd).


![](NetBSD.png)




